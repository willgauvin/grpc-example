from ska_pst_grpc.service.service import Client, Server

def main():
    import argparse
    
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("-s", "--server", action="store_true", default=False)
    arg_parser.add_argument("--host", type=str, required=False)
    arg_parser.add_argument("--port", type=int, default=51000)
    arg_parser.add_argument("-t", "--telemetry", action="store_true", default=False)
    arg_parser.add_argument("-n", "--num-times", required=False, type=int)
    arg_parser.add_argument("name", nargs="?")
    
    args = arg_parser.parse_args()
    if args.server:
        server = Server(**vars(args))
        server.serve()
    else:
        assert args.host is not None, "Host name is needed for clients."

        client = Client(**vars(args))
        client.say_hello(args.name)
        if args.telemetry:
            for t in client.monitor(num_times=args.num_times):
                print(f"Recevied telemetry: {t}")

if __name__ == "__main__":
    main()
