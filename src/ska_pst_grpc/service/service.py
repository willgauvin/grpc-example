from __future__ import annotations
from dataclasses import dataclass
import random
from typing import Optional, Generator
from unicodedata import name

from ska_pst_grpc.proto.hello_world_pb2 import HelloWorldRequest, HelloWorldResponse, MonitorRequest, MonitorResponse
from ska_pst_grpc.proto.hello_world_pb2_grpc import HelloWorldServiceServicer, HelloWorldServiceStub, add_HelloWorldServiceServicer_to_server
import grpc
from concurrent import futures

import enum

class ServerStatus(enum.IntEnum):
    OK = 0
    WARNING = 1
    ERROR = 2
    FAULT = 3

@dataclass(repr=False)
class MonitorData:
    received_data: int
    received_rate: float
    status: ServerStatus

    def __repr__(self: MonitorData):
        return f"received_data={self.received_data}, received_rate={self.received_rate:.3f}, status={self.status.name}"

class Client:
    
    def __init__(self: Client, host: str, port: int, **kwargs) -> None:
        print(f"Connecting to {host}:{port}")
        self.channel = grpc.insecure_channel(f"{host}:{port}")
        self.stub = HelloWorldServiceStub(channel=self.channel)

    def say_hello(self: Client, name: Optional[str] = None) -> None:
        request = HelloWorldRequest(
            name=name
        )
        response = self.stub.sayHello(request)
        print(f"{response.text}")

    def monitor(self: Client, num_times: Optional[int] = None) -> Generator[MonitorData, None, None]:
        try:
            for t in self.stub.monitor(MonitorRequest(num_times=num_times)):
                status = ServerStatus(t.status)
                
                yield MonitorData(
                    received_data=t.received_data,
                    received_rate=t.received_rate,
                    status=status,
                )
        except grpc.RpcError as e:
            print(f"Exception raised while monitoring. {e}")

class _Servicer(HelloWorldServiceServicer):
    
    def sayHello(self, request: HelloWorldRequest, context: grpc.ServicerContext) -> HelloWorldResponse:
        name = request.name
        print(f"Received request with name='{name}'")
        if name:
            output = f"Hello {name}!"
        else:
            output = f"Hellow World!"

        print(f"Returning: '{output}'")        
        return HelloWorldResponse(text=output)

    def monitor(self, request: MonitorRequest, context: grpc.ServicerContext) -> Generator[MonitorResponse]:
        # return super().get_telem(request, context)
        num_times = request.num_times
        if num_times == 0:
            import sys
            num_times = sys.maxsize 
       
        received_data = 0
        received_rate = 0.0
        total_wait = 0

        for i in range(num_times):
            import time
            next_wait = (random.random() + 1.0) / 2
            time.sleep(next_wait)
            total_wait += next_wait
            
            received_data += random.randint(0, 100)
            received_rate = received_data/total_wait

            status = MonitorResponse.Status.OK
            if ((i + 1) % 5) == 0:
                status = MonitorResponse.Status.WARNING
            elif ((i + 1) % 11) == 0:
                status = MonitorResponse.Status.ERROR
            elif ((i + 1) % 19) == 0:
                status = MonitorResponse.Status.FAULT

            yield MonitorResponse(
                received_data=received_data,
                received_rate=received_rate,
                status=status,
            )

class Server:
    
    def __init__(self: Server, port: int, max_workers: int = 10, **kargs) -> None:
        self._thread_pool = futures.ThreadPoolExecutor(max_workers=max_workers)
        self._server = grpc.server(self._thread_pool)
        self._port = port
        add_HelloWorldServiceServicer_to_server(
            _Servicer(),
            server=self._server
        )
        
        
    def serve(self: Server) -> None:
        print(f"Listening on port {self._port}")
        self._server.add_insecure_port(f"0.0.0.0:{self._port}")
        self._server.start()
        try:
            self._server.wait_for_termination()
        except KeyboardInterrupt:
            print("Exiting")
