ARG ALPINE_VERSION=3.16.0
FROM alpine:${ALPINE_VERSION} AS baseimage
ARG UID=1000
ARG GID=1000
ARG UNAME=pst

# set up the user
RUN apk add --no-cache protobuf grpc && \
    addgroup -g $GID $UNAME && \
    adduser -u $UID -G $UNAME -h /home/$UNAME -D $UNAME && \
    mkdir /app && \
    chown $UID:$GID /app

FROM baseimage AS grpc-generator
ARG UNAME=pst

RUN apk add --no-cache protobuf-dev grpc-dev

USER $UNAME

WORKDIR /app

COPY ./proto/ /app/proto/

RUN mkdir -p /app/python && \
    mkdir -p /app/cpp && \
    protoc --proto_path=$(pwd)/proto \
    --python_out=$(pwd)/python \
    --grpc_python_out=$(pwd)/python \
    --cpp_out=$(pwd)/cpp \
    --grpc_cpp_out=$(pwd)/cpp \
    --plugin=protoc-gen-grpc_cpp=$(which grpc_cpp_plugin) \
    --plugin=protoc-gen-grpc_python=$(which grpc_python_plugin) \
    $(find $(pwd)/proto/ -iname "*.proto")

FROM baseimage AS python-image
ARG POETRY_HOME=/opt/poetry
ARG UID=1000
ARG GID=1000
ARG UNAME=pst

SHELL ["/bin/ash", "-o", "pipefail", "-c"]
RUN apk add --no-cache python3 py3-pip && \
    apk add --no-cache --virtual .tmp-tools curl gcc musl-dev python3-dev libffi-dev && \
    curl -sSL https://install.python-poetry.org/ | POETRY_HOME=${POETRY_HOME} python3 - && \
    chmod a+x ${POETRY_HOME}/bin/poetry && \
    apk del .tmp-tools

WORKDIR /app
COPY --chown=${UID}:${GID} pyproject.toml poetry.lock* /app/

RUN ${POETRY_HOME}/bin/poetry export \
        --format requirements.txt \
        --output poetry-requirements.txt --without-hashes && \
    pip3 install -r poetry-requirements.txt && \
    rm poetry-requirements.txt 

COPY --chown=$UID:$GID ./src /app/src
COPY --from=grpc-generator --chown=$UID:$GID /app/python /app/src

USER ${UNAME}

ENV PYTHONPATH=/app/src:/usr/lib/python3.10/site-packages

ENTRYPOINT [ "python3", "-m", "ska_pst_grpc.service" ]

FROM baseimage AS cpp-builder
ARG UID=1000
ARG GID=1000
ARG UNAME=pst

SHELL ["/bin/ash", "-o", "pipefail", "-c"]
RUN apk add --no-cache --virtual .tmp-tools build-base protobuf-dev grpc-dev re2-dev c-ares-dev pkgconfig cmake

USER ${UNAME}

WORKDIR /app

COPY --chown=${UID}:${GID} ./cpp /app/cpp
COPY --chown=${UID}:${GID} CMakeLists.txt /app/
COPY --from=grpc-generator --chown=${UID}:${GID} /app/cpp /app/cpp

RUN mkdir -p /app/build && \
    cd /app/build && \
    cmake .. && \
    make -j$(nproc)

FROM baseimage AS grpc-server
ARG UID=1000
ARG GID=1000
ARG UNAME=pst

USER ${UNAME}

COPY --from=cpp-builder --chown=${UID}:${GID} /app/build/grpc_service /app/grpc_service

WORKDIR /app
ENTRYPOINT [ "./grpc_service" ]

# USER $UNAME

# ENV PYTHONPATH=/app/src:/usr/lib/python3.10/site-packages

# ENTRYPOINT [ "python3", "-m", "ska_pst_grpc.service" ]

# FROM 
# FROM baseimage as python-base
# ARG POETRY_HOME=/opt/poetry

# SHELL ["/bin/ash", "-o", "pipefail", "-c"]
# RUN apk add --no-cache python3 curl && \
#     apk add --no-cache --virtual .tmp-tools gcc musl-dev python3-dev libffi-dev && \
#     curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python3 - && \
#     chmod a+x ${POETRY_HOME}/bin/poetry && \
#     apk del .tmp-tools

# FROM python-base as python-protobuf-builder

# RUN mkdir -p /app && \
#     mkdir -p /app/generated && \
#     apk add --no-cache libprotobuf && \
#     apk add --no-cache --virtual .tmp-tools protoc

# WORKDIR /app

