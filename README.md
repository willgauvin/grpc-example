# grpc-example

# Description

This is example code used to spike gRPC to be used for communicating between Python clients and C++
processes.

This uses code generated from Protobuf and gRPC to test out a simple request/response command, such as
hello world, but also to test streaming of messages back from the server to simulate monitoring and getting
telemetry data from the server.

# Setup

You need to have the followin installed locally:

* cmake
* protobuf
* gRPC

Depending on your development environment you should also include the development versions.

You can also use Docker to build and run this code.

# Docker

This project uses a multi-state Dockerfile where there are 2 main targets, one for the client
and one for the server.

To run both the client and server and have them communicate with each other you will need to
create a Docker network

```
docker network create --driver bridge grpc-network
```

## Server

Build the server with:
```
docker build --target grpc-server -t grpc-server .
```

Run the server with:
```
docker run -it --rm --network grpc-network --name grpc-server grpc-server
```

To get help of the server run:
```
docker run -it --rm grpc-server -h
```


## Client

Build the client with:
```
docker build --target python-image -t grpc-client .
```

Run the client with:
```
docker run -it --rm --network grpc-network  grpc-client --host grpc-server -t -n 10
```

To get help of the client run:
```
docker run -it --rm grpc-client -h
```

## License
This is licensed with Apache 2, for more details view the [LICENSE](LICENSE) file.
