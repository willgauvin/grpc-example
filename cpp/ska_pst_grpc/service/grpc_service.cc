/*
 *
 * Copyright 2015, Google Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This has been modified to meet the requirements of this example
 * but the code is based from original Google RPC C++ example.
 */

#include <unistd.h>
#include <iostream>
#include <memory>
#include <string>
#include <stdlib.h>
#include <thread>
#include <random>
#include <chrono>

#include <grpc++/grpc++.h>

#include "ska_pst_grpc/proto/hello_world.grpc.pb.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerWriter;
using grpc::Status;
using ska_pst_grpc::proto::HelloWorldRequest;
using ska_pst_grpc::proto::HelloWorldResponse;
using ska_pst_grpc::proto::HelloWorldService;
using ska_pst_grpc::proto::MonitorRequest;
using ska_pst_grpc::proto::MonitorResponse;
using ska_pst_grpc::proto::MonitorResponse_Status;
using ska_pst_grpc::proto::MonitorResponse_Status_OK;
using ska_pst_grpc::proto::MonitorResponse_Status_WARNING;
using ska_pst_grpc::proto::MonitorResponse_Status_ERROR;
using ska_pst_grpc::proto::MonitorResponse_Status_FAULT;

// Logic and data behind the server's behavior.
class HelloWorldServiceImpl final : public HelloWorldService::Service {
  Status sayHello(ServerContext* context, const HelloWorldRequest* request,
                  HelloWorldResponse* reply) override {
    std::string prefix("Hello ");
    std::string output;
    if (request->name().empty()) {
      output = prefix + "World!";
    } else {
      output = prefix + request->name();
    }

    std::cout << "Saying: " << output << std::endl;
    reply->set_text(output);
    
    return Status::OK;
  }

  Status monitor(ServerContext* context, const MonitorRequest* request, ServerWriter<MonitorResponse>* writer) override {
    std::mt19937_64 eng{std::random_device{}()};  // or seed however you want
    std::uniform_int_distribution<> dist{100, 1000};

    auto num_times = request->num_times();
    if (num_times == 0) {
      num_times = 10;
    }

    int64_t received_data;
    double received_rate;
    double total_wait;

    for (auto i = 0; i<num_times; i++)
    {
      if (context->IsCancelled()) {
        std::cout << "Context has cancelled. Exiting loop" << std::endl;
        break;
      }

      auto next_sleep = dist(eng);
      total_wait += (next_sleep / 1000.0);
      std::cout << "Sleeping " << next_sleep << "ms" << std::endl;
      std::this_thread::sleep_for(std::chrono::milliseconds{next_sleep});

      received_data += (rand() % 100);
      received_rate = received_data / total_wait;

      MonitorResponse_Status status = MonitorResponse_Status_OK;
      if (((i + 1) % 5) == 0) {
        status = MonitorResponse_Status_WARNING;
      } else if (((i + 1) % 11) == 0) {
        status = MonitorResponse_Status_ERROR;
      } else if (((i + 1) % 11) == 0) {
        status = MonitorResponse_Status_FAULT;
      }

      MonitorResponse response;
      response.set_received_data(received_data);
      response.set_received_rate(received_rate);
      response.set_status(status);

      std::cout << "Sending response: \n" << response.DebugString() << std::endl;
      if (!writer->Write(response)) {
        std::cout << "Writing to writer returned false. Exiting" << std::endl;
        break;
      }
    }

    return Status::OK;
  }
};

void run_server(int port) {
  std::string server_address("0.0.0.0:");
  server_address.append(std::to_string(port));
  HelloWorldServiceImpl service;

  ServerBuilder builder;
  // Listen on the given address without any authentication mechanism.
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  // Register "service" as the instance through which we'll communicate with
  // clients. In this case it corresponds to an *synchronous* service.
  builder.RegisterService(&service);
  // Finally assemble the server.
  std::unique_ptr<Server> server(builder.BuildAndStart());
  std::cout << "Server listening on " << server_address << std::endl;

  // Wait for the server to shutdown. Note that some other thread must be
  // responsible for shutting down the server for this call to ever return.
  server->Wait();
}

void usage() 
{
  std::cout << "Usage:\n"
    "grpc_server [options]\n"
    "Use to receive gRPC commands. Acts as a greeter service but can also\n"
    "simulate sending monitor data.\n\n"
    "Quit with CTRL+C.\n"
    "\n"
    "  -h          print this help text\n"
    "  -p          port number to run on (default 51000)\n"
    << std::endl;
}

int main(int argc, char** argv) {
  int port = 51000;
  int c;

  while ((c = getopt(argc, argv, "p:h")) != EOF)
  {
    switch(c)
    {
      case 'p':
        port = atoi(optarg);
        break;

      case 'h':
        usage();
        exit(EXIT_SUCCESS);

      default:
        std::cerr << "Unrecognised option [" << c << "]" << std::endl;
        usage();
        return EXIT_FAILURE;
        break;
    }
  }

  run_server(port);

  return 0;
}
